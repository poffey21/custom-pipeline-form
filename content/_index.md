---
title: "SA Triage Request"
date: "2020-02-25"
---


<form id="requestform" method="post" action="#">
  <div class="form-group row">
    <label for="company" class="col-4 col-form-label">Company</label>
    <div class="col-8">
      <div class="input-group">
        <div class="input-group-addon">
          <i class="fa fa-building"></i>
        </div>
        <input id="company" name="company" placeholder="Acme Solutions" type="text" required="required" class="form-control">
      </div>
    </div>
  </div>
  <div class="form-group row">
    <label for="size" class="col-4 col-form-label">Opportunity Size</label>
    <div class="col-8">
      <div class="input-group">
        <div class="input-group-addon">
          <i class="fa fa-id-badge"></i>
        </div>
        <input id="size" name="size" placeholder="50 Premium" type="text" required="required" class="form-control">
      </div>
      <small id="sizeHelp" class="form-text text-muted">Opportunities need to be at 45 Premium or 10 Ultimate.</small>
    </div>
  </div>
  
  <div class="form-group row">
    <label for="commandplan" class="col-4 col-form-label">Command Plan</label>
    <div class="col-8">
      <div class="input-group">
        <div class="input-group-addon">
          <i class="fa fa-id-terminal"></i>
        </div>
        <input id="commandplan" name="commandplan" placeholder="SFDC Command Plan Link" type="text" required="required" class="form-control">
      </div>
      <small id="commandplanHelp" class="form-text text-muted">Most information necessary should be within the command plan and will help minimize the amount of copy/pasting the AEs need to do.</small>
    </div>
  </div>

  <div class="form-group row form-check">
    <div class="col-8">
      <input name="location" type="radio"  value="SaaS" class="form-check-input">SaaS</input>
      <input name="location" type="radio"  value="Self-Managed" class="form-check-input">Self-Managed</input>
      <input name="location" type="radio"  value="Undecided" class="form-check-input">Undecided</input>
      <small id="locationHelp" class="form-text text-muted">
        Knowing where they plan to use GitLab will shape the conversation we have with them.
      </small>
    </div>
  </div>

  <div class="form-group row form-check">
    <label for="whynow" class="col-4 col-form-label">Command Plan's Why Now</label>
    <div class="col-8">
      <input id="whynow" name="whynow" type="checkbox"  value="[x]" required="required" class="form-check-input">
      <small id="sizeHelp" class="form-text text-muted">
        The <a target="_blank" href="https://about.gitlab.com/handbook/sales/command-of-the-message/command-plan/#opportunity-overview"><strong>Why Now</strong></a> is in sentence form and describes the compelling event and why the customer needs to make the purchase now.
      </small>
    </div>
  </div>

  <div class="form-group row form-check">
    <label for="identifypain" class="col-4 col-form-label">MEDDPPICC's Identify Pain</label>
    <div class="col-8">
      <input id="identifypain" name="identifypain" type="checkbox"  value="[x]" required="required" class="form-check-input">
      <small id="sizeHelp" class="form-text text-muted">
        The <a target="_blank" href="https://about.gitlab.com/handbook/sales/meddppicc/#identify-pain"><strong>Identified Pain</strong></a> is in sentence form mapped to the people who have the pain and in charge of fixing it.
      </small>
    </div>
  </div>
  
  <div class="form-group row">
    <label for="agenda" class="col-4 col-form-label">Pitch Deck Contains Customer-Agreed Upon Agenda</label>
    <div class="col-8">
      <input id="agenda" name="agenda" type="checkbox"  value="[x]" required="required" class="form-check-input">
      <small id="sizeHelp" class="form-text text-muted">
        The <a target="_blank" href="https://about.gitlab.com/handbook/sales/commercial/#custom-deck-requirements"><strong>Customer Agreed-Upon Meeting Agenda</strong></a> is in Customer Pitch Deck (and is available for SAs to edit) or listed below for deals less than $10k.
      </small>
    </div>
  </div>

  <div class="form-group row">
    <small class="form-text text-muted">
      ☝ SAs will leverage SFDC chatter to ask for clarifications from the AE & 
      ASM when the 3 items above are not providing enough information. Within 
      the chatter, the SA will make a best effort to provide the why and 
      recommend questions to ask the customer. The reason for this is because 
      when information is not provided, it limits an SA's ability to prepare 
      for the meeting with the customer and will inevitably increase the sales 
      cycle.
    </small>
  </div>

  <div class="form-group row">
    <label for="timeframe" class="col-4 col-form-label">Preferred Date & Time Options</label>
    <div class="col-8">
      <textarea id="timeframe" name="timeframe" cols="40" rows="10" placeholder="- today at 3pm EST&#10;- tomorrow at 4pm EST" class="form-control"></textarea>
      <small id="timeframeHelp" class="form-text text-muted">Today is Having multiple options will help maximize the number of SAs that can join the call.</small>
    </div>
  </div>

  <div class="form-group row">
    <label for="extra" class="col-4 col-form-label">Additional Details</label>
    <div class="col-8">
      <textarea id="extra" name="extra" cols="40" rows="10" placeholder="What are the questions the prospects want answered in the next meeting or specific items to be demoed?" class="form-control"></textarea>
      <small id="sizeHelp" class="form-text text-muted">Any additional details that are relevant can be provided here.</small>
    </div>
  </div>
  <div class="form-group row">
    <div class="offset-4 col-8">
      <button type="button" id="api-example">Create the Triage Request</button>
    </div>
  </div>
  <div id="pipeline-after" style="display:none;" class="form-group row">
    <p id="api-output">Please stand by...</p>
  </div>
</form>
