# Custom Pipeline Form

Shows a form. Sign in to GitLab and then click the button to run a pipeline in the target repo.

The most important pages to modify:

* `content/_index.md`: All of the Form Fields
* `layouts/partials/footer_customer.html`: The Javascript that will pull data from the Form Fields, Create the Issue, and then Redirect User

Only changes to master will publish a new page.

## Useful Setup Links

* https://www.ilovedevops.com/post/2019-08-04-authenticate-gitlab-api-pages-site/
* https://docs.gitlab.com/ee/api/oauth2.html#implicit-grant-flow
* https://www.youtube.com/watch?v=7NsyCYmVQKw
* https://gitlab.com/-/profile/applications
* https://docs.gitlab.com/ee/api/issues.html#new-issue
* https://getbootstrap.com/docs/4.0/components/forms/#checkboxes-and-radios
* https://gitlab.com/gitlab-com/customer-success/sa-triage-boards/commercial-triage/-/issues/new?issuable_template=SA%20Activity
* https://fontawesome.com/v5.15/icons?d=gallery&p=2&q=count&m=free
